#!/usr/bin/env bash
#
# Configure git to allow push back to the remote repository.
#
# Required globals:
#   PRIVATE_KEY: base64 encoded ssh private key
#

set -e

# Configure git
git config user.name "Bitbucket Pipelines Pipes"
git config user.email commits-noreply@bitbucket.org
git remote set-url origin ${BITBUCKET_GIT_SSH_ORIGIN}
