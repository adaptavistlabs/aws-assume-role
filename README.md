# Bitbucket Pipelines Pipe: AWS Assume Role
Assume a role and store credentials in an env file to use in other pipes

## YAML Definition
Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: adaptavist/aws-assume-role:0.0.3
  variables:
    AWS_ACCESS_KEY_ID: '<string>'
    AWS_SECRET_ACCESS_KEY: '<string>'
    AWS_DEFAULT_REGION: '<string>'
    AWS_ROLE_ARN: '<string>'

    # Common variables
    # AWS_SESSION_NAME: '<string>' # Optional.
    # DURATION: '<integer>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables
The following variables are required:

| Variable | Usage |
| ----------- | ----- |
| AWS_ACCESS_KEY_ID (*)     | AWS access key. |
| AWS_SECRET_ACCESS_KEY (*) | AWS secret key. |
| AWS_DEFAULT_REGION (*)    | The AWS region code (us-east-1, us-west-2, etc.) of the region containing the AWS resource(s). For more information, see [Regions and Endpoints](https://docs.aws.amazon.com/general/latest/gr/rande.html) in the _Amazon Web Services General Reference_. |
| AWS_ROLE_ARN (*)          | AWS ARN of the role to assume. |
| AWS_SESSION_NAME          | Name of the session in AWS. Default: `"${BITBUCKET_WORKSPACE}-${BITBUCKET_REPO_SLUG}-${BITBUCKET_BUILD_NUMBER}-`date +%Y%m%d`"`.|
| DURATION                  | Lifetime of the session token in seconds. Default: `3600`.|
| DEBUG                     | Turn on extra debug information. Default: `false`.|
_(*) = required variable._

## Details


## Prerequisites
You will need:

  * An IAM user configured with programmatic access that can perform the following operations on the target role arn:
    * sts:AssumeRole

## Task Output
The pipe will create a `aws-credentials.env` file under the `$BITBUCKET_PIPE_SHARED_STORAGE_DIR` directory.

The credentials can be read/sourced from this file, and used in later pipes or commands.


[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,aws
