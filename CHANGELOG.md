# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.3

- patch: Ensure that pipe shell scripts are executable

## 0.0.2

- patch: Fix pipe.yml docker repository

## 0.0.1

- patch: initial release

