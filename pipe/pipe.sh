#!/bin/bash
#
# Assumes a IAM role and writes a session credentials to an file to be sourced by other pipes
# See https://docs.aws.amazon.com/cli/latest/reference/sts/index.html
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   AWS_ROLE_ARN
#
# Optional globals:
#   AWS_SESSION_NAME
#   DURATION
#   DEBUG (true or false)

SCRIPT_PATH=$(dirname "$0")
source "${SCRIPT_PATH}"/common.sh

parse_environment_variables() {
    # Required variables
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  AWS_ROLE_ARN=${AWS_ROLE_ARN:?'AWS_ROLE_ARN variable missing.'}

  # Default variables
  AWS_SESSION_NAME="${BITBUCKET_WORKSPACE}-${BITBUCKET_REPO_SLUG}-${BITBUCKET_BUILD_NUMBER}-`date +%Y%m%d`"
  DURATION="${DURATION:-3600}"

}

enable_debug
parse_environment_variables

set -e


info 'Assuming role: $role_arn...'
sts=( $(
    aws sts assume-role \
    --role-arn "$AWS_ROLE_ARN" \
    --role-session-name "$AWS_SESSION_NAME" \
    --query 'Credentials.[AccessKeyId,SecretAccessKey,SessionToken]' \
    --duration-seconds "$DURATION" \
    --output text
) )

echo "export AWS_ACCESS_KEY_ID=${sts[0]}" > $BITBUCKET_PIPE_STORAGE_DIR/aws-credentials.env
echo "export AWS_SECRET_ACCESS_KEY=${sts[1]}" >> $BITBUCKET_PIPE_STORAGE_DIR/aws-credentials.env
echo "export AWS_SESSION_TOKEN=${sts[2]}" >> $BITBUCKET_PIPE_STORAGE_DIR/aws-credentials.env
echo "export AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}" >> $BITBUCKET_PIPE_STORAGE_DIR/aws-credentials.env

success "Wrote $BITBUCKET_PIPE_STORAGE_DIR/aws-credentials.env with sts credentials"
